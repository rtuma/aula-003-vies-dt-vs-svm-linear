```python
from IPython.core.display import display, HTML
display(HTML("<style>.container { width:76% !important; float:right }</style>"))
```


<style>.container { width:76% !important; float:right }</style>



```python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
```


```python
from sklearn.datasets import make_classification, make_moons
```


```python

```


```python
def make_data(n_samples=150, noise=.5):
    y = np.zeros(n_samples)
    y[n_samples//2:] = 1.
    
    X = np.zeros((n_samples,2))
    
    X[y==0,0] = np.random.randn((y==0).sum())
    X[y==1,0] = np.random.randn((y==1).sum())+.5
    
    X[y==0,1] = X[y==0,0]*(-3.)+1.5 + np.random.randn((y==0).sum())*noise
    X[y==1,1] = X[y==1,0]*(-3.)-.5 + np.random.randn((y==0).sum())*noise
    
    return X,y
```


```python
#X, y = make_classification(n_features=2, n_redundant=0, n_informative=2,
#                           random_state=1, n_clusters_per_class=1,class_sep=1.,hypercube=False)

#X,y = make_moons(n_samples=1000,noise=.4)

X,y = make_data()

plt.figure(figsize=(15,5))
plt.plot(X[y==0,0],X[y==0,1],'C0o')
plt.plot(X[y==1,0],X[y==1,1],'C3o')
```




    [<matplotlib.lines.Line2D at 0x7f1b0d9ac190>]




    
![png](DT%20vs%20SVM%20Linear_files/DT%20vs%20SVM%20Linear_5_1.png)
    



```python
from sklearn.model_selection import GridSearchCV, train_test_split

from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC

X_tr, X_ts, y_tr, y_ts = train_test_split(X,y, test_size=.3, random_state=61658)

params1 = {
    'max_leaf_nodes':[2,3,4,5,6,7,8,9,10,11,12,16,32,45,64],
    'criterion':['gini','entropy'],
    'class_weight':[None,'balanced'],
}
grid1 = GridSearchCV(DecisionTreeClassifier(random_state=61658),params1, cv=30, scoring='roc_auc',verbose=5)
grid1.fit(X_tr, y_tr)

params2 = {
    'C':np.logspace(-5,5,30,base=2),
}
grid2 = GridSearchCV(SVC(kernel='linear',random_state=61658,probability=True),params2, cv=30, scoring='roc_auc',verbose=5)
grid2.fit(X_tr, y_tr)
```

    Fitting 30 folds for each of 60 candidates, totalling 1800 fits
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 1/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 2/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 3/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 4/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 5/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 6/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 7/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 8/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 9/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 10/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 11/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 12/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 13/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 14/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 15/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 16/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 17/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 18/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 19/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 20/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 21/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 22/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 23/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 24/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 25/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 26/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 27/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 28/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 29/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 30/30] END class_weight=None, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=2; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=3; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=4; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=5; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=6; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=7; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=8; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=9; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=10; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=11; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=12; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=16; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=32; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=45; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=gini, max_leaf_nodes=64; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=2; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=3; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=4; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=5; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=6; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=7; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=8; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=9; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=10; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=11; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=12; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=16; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=32; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=45; total time=   0.0s
    [CV 1/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 2/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 3/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 4/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 5/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 6/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 7/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 8/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 9/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 10/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 11/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 12/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 13/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 14/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 15/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 16/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 17/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 18/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 19/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 20/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 21/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 22/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 23/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 24/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 25/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 26/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 27/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 28/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 29/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    [CV 30/30] END class_weight=balanced, criterion=entropy, max_leaf_nodes=64; total time=   0.0s
    Fitting 30 folds for each of 30 candidates, totalling 900 fits
    [CV 1/30] END .....................................C=0.03125; total time=   0.0s
    [CV 2/30] END .....................................C=0.03125; total time=   0.0s
    [CV 3/30] END .....................................C=0.03125; total time=   0.0s
    [CV 4/30] END .....................................C=0.03125; total time=   0.0s
    [CV 5/30] END .....................................C=0.03125; total time=   0.0s
    [CV 6/30] END .....................................C=0.03125; total time=   0.0s
    [CV 7/30] END .....................................C=0.03125; total time=   0.0s
    [CV 8/30] END .....................................C=0.03125; total time=   0.0s
    [CV 9/30] END .....................................C=0.03125; total time=   0.0s
    [CV 10/30] END ....................................C=0.03125; total time=   0.0s
    [CV 11/30] END ....................................C=0.03125; total time=   0.0s
    [CV 12/30] END ....................................C=0.03125; total time=   0.0s
    [CV 13/30] END ....................................C=0.03125; total time=   0.0s
    [CV 14/30] END ....................................C=0.03125; total time=   0.0s
    [CV 15/30] END ....................................C=0.03125; total time=   0.0s
    [CV 16/30] END ....................................C=0.03125; total time=   0.0s
    [CV 17/30] END ....................................C=0.03125; total time=   0.0s
    [CV 18/30] END ....................................C=0.03125; total time=   0.0s
    [CV 19/30] END ....................................C=0.03125; total time=   0.0s
    [CV 20/30] END ....................................C=0.03125; total time=   0.0s
    [CV 21/30] END ....................................C=0.03125; total time=   0.0s
    [CV 22/30] END ....................................C=0.03125; total time=   0.0s
    [CV 23/30] END ....................................C=0.03125; total time=   0.0s
    [CV 24/30] END ....................................C=0.03125; total time=   0.0s
    [CV 25/30] END ....................................C=0.03125; total time=   0.0s
    [CV 26/30] END ....................................C=0.03125; total time=   0.0s
    [CV 27/30] END ....................................C=0.03125; total time=   0.0s
    [CV 28/30] END ....................................C=0.03125; total time=   0.0s
    [CV 29/30] END ....................................C=0.03125; total time=   0.0s
    [CV 30/30] END ....................................C=0.03125; total time=   0.0s
    [CV 1/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 2/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 3/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 4/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 5/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 6/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 7/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 8/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 9/30] END .........................C=0.03968747494481696; total time=   0.0s
    [CV 10/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 11/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 12/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 13/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 14/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 15/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 16/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 17/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 18/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 19/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 20/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 21/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 22/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 23/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 24/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 25/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 26/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 27/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 28/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 29/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 30/30] END ........................C=0.03968747494481696; total time=   0.0s
    [CV 1/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 2/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 3/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 4/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 5/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 6/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 7/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 8/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 9/30] END ........................C=0.050403061359855166; total time=   0.0s
    [CV 10/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 11/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 12/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 13/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 14/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 15/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 16/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 17/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 18/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 19/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 20/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 21/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 22/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 23/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 24/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 25/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 26/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 27/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 28/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 29/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 30/30] END .......................C=0.050403061359855166; total time=   0.0s
    [CV 1/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 2/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 3/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 4/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 5/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 6/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 7/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 8/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 9/30] END .........................C=0.06401184751556233; total time=   0.0s
    [CV 10/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 11/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 12/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 13/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 14/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 15/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 16/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 17/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 18/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 19/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 20/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 21/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 22/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 23/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 24/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 25/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 26/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 27/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 28/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 29/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 30/30] END ........................C=0.06401184751556233; total time=   0.0s
    [CV 1/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 2/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 3/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 4/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 5/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 6/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 7/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 8/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 9/30] END .........................C=0.08129499502225035; total time=   0.0s
    [CV 10/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 11/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 12/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 13/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 14/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 15/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 16/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 17/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 18/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 19/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 20/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 21/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 22/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 23/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 24/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 25/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 26/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 27/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 28/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 29/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 30/30] END ........................C=0.08129499502225035; total time=   0.0s
    [CV 1/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 2/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 3/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 4/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 5/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 6/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 7/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 8/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 9/30] END .........................C=0.10324457849870658; total time=   0.0s
    [CV 10/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 11/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 12/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 13/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 14/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 15/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 16/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 17/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 18/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 19/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 20/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 21/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 22/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 23/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 24/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 25/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 26/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 27/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 28/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 29/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 30/30] END ........................C=0.10324457849870658; total time=   0.0s
    [CV 1/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 2/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 3/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 4/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 5/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 6/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 7/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 8/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 9/30] END ..........................C=0.1311205319153793; total time=   0.0s
    [CV 10/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 11/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 12/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 13/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 14/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 15/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 16/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 17/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 18/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 19/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 20/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 21/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 22/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 23/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 24/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 25/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 26/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 27/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 28/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 29/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 30/30] END .........................C=0.1311205319153793; total time=   0.0s
    [CV 1/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 2/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 3/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 4/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 5/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 6/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 7/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 8/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 9/30] END ...........................C=0.166522970404566; total time=   0.0s
    [CV 10/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 11/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 12/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 13/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 14/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 15/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 16/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 17/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 18/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 19/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 20/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 21/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 22/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 23/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 24/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 25/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 26/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 27/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 28/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 29/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 30/30] END ..........................C=0.166522970404566; total time=   0.0s
    [CV 1/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 2/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 3/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 4/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 5/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 6/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 7/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 8/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 9/30] END .........................C=0.21148403890136672; total time=   0.0s
    [CV 10/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 11/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 12/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 13/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 14/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 15/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 16/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 17/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 18/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 19/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 20/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 21/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 22/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 23/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 24/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 25/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 26/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 27/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 28/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 29/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 30/30] END ........................C=0.21148403890136672; total time=   0.0s
    [CV 1/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 2/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 3/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 4/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 5/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 6/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 7/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 8/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 9/30] END ...........................C=0.268584559844054; total time=   0.0s
    [CV 10/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 11/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 12/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 13/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 14/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 15/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 16/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 17/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 18/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 19/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 20/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 21/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 22/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 23/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 24/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 25/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 26/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 27/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 28/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 29/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 30/30] END ..........................C=0.268584559844054; total time=   0.0s
    [CV 1/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 2/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 3/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 4/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 5/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 6/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 7/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 8/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 9/30] END ..........................C=0.3411021756600186; total time=   0.0s
    [CV 10/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 11/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 12/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 13/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 14/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 15/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 16/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 17/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 18/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 19/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 20/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 21/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 22/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 23/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 24/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 25/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 26/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 27/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 28/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 29/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 30/30] END .........................C=0.3411021756600186; total time=   0.0s
    [CV 1/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 2/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 3/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 4/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 5/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 6/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 7/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 8/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 9/30] END ..........................C=0.4331994896041452; total time=   0.0s
    [CV 10/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 11/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 12/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 13/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 14/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 15/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 16/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 17/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 18/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 19/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 20/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 21/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 22/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 23/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 24/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 25/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 26/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 27/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 28/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 29/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 30/30] END .........................C=0.4331994896041452; total time=   0.0s
    [CV 1/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 2/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 3/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 4/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 5/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 6/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 7/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 8/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 9/30] END ..........................C=0.5501630044727043; total time=   0.0s
    [CV 10/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 11/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 12/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 13/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 14/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 15/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 16/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 17/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 18/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 19/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 20/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 21/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 22/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 23/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 24/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 25/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 26/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 27/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 28/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 29/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 30/30] END .........................C=0.5501630044727043; total time=   0.0s
    [CV 1/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 2/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 3/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 4/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 5/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 6/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 7/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 8/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 9/30] END ..........................C=0.6987065745784214; total time=   0.0s
    [CV 10/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 11/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 12/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 13/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 14/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 15/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 16/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 17/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 18/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 19/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 20/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 21/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 22/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 23/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 24/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 25/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 26/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 27/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 28/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 29/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 30/30] END .........................C=0.6987065745784214; total time=   0.0s
    [CV 1/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 2/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 3/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 4/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 5/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 6/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 7/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 8/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 9/30] END ...........................C=0.887356789515519; total time=   0.0s
    [CV 10/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 11/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 12/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 13/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 14/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 15/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 16/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 17/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 18/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 19/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 20/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 21/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 22/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 23/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 24/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 25/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 26/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 27/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 28/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 29/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 30/30] END ..........................C=0.887356789515519; total time=   0.0s
    [CV 1/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 2/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 3/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 4/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 5/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 6/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 7/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 8/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 9/30] END ...........................C=1.126942411232332; total time=   0.0s
    [CV 10/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 11/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 12/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 13/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 14/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 15/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 16/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 17/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 18/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 19/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 20/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 21/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 22/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 23/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 24/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 25/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 26/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 27/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 28/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 29/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 30/30] END ..........................C=1.126942411232332; total time=   0.0s
    [CV 1/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 2/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 3/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 4/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 5/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 6/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 7/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 8/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 9/30] END ..........................C=1.4312159587211133; total time=   0.0s
    [CV 10/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 11/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 12/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 13/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 14/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 15/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 16/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 17/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 18/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 19/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 20/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 21/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 22/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 23/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 24/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 25/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 26/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 27/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 28/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 29/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 30/30] END .........................C=1.4312159587211133; total time=   0.0s
    [CV 1/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 2/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 3/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 4/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 5/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 6/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 7/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 8/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 9/30] END ...........................C=1.817643120075724; total time=   0.0s
    [CV 10/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 11/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 12/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 13/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 14/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 15/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 16/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 17/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 18/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 19/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 20/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 21/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 22/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 23/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 24/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 25/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 26/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 27/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 28/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 29/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 30/30] END ..........................C=1.817643120075724; total time=   0.0s
    [CV 1/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 2/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 3/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 4/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 5/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 6/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 7/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 8/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 9/30] END ...........................C=2.308405305171975; total time=   0.0s
    [CV 10/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 11/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 12/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 13/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 14/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 15/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 16/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 17/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 18/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 19/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 20/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 21/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 22/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 23/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 24/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 25/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 26/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 27/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 28/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 29/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 30/30] END ..........................C=2.308405305171975; total time=   0.0s
    [CV 1/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 2/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 3/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 4/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 5/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 6/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 7/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 8/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 9/30] END ...........................C=2.931672886767848; total time=   0.0s
    [CV 10/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 11/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 12/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 13/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 14/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 15/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 16/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 17/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 18/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 19/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 20/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 21/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 22/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 23/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 24/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 25/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 26/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 27/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 28/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 29/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 30/30] END ..........................C=2.931672886767848; total time=   0.0s
    [CV 1/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 2/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 3/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 4/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 5/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 6/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 7/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 8/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 9/30] END ..........................C=3.7232222156799417; total time=   0.0s
    [CV 10/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 11/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 12/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 13/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 14/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 15/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 16/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 17/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 18/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 19/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 20/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 21/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 22/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 23/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 24/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 25/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 26/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 27/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 28/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 29/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 30/30] END .........................C=3.7232222156799417; total time=   0.0s
    [CV 1/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 2/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 3/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 4/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 5/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 6/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 7/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 8/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 9/30] END ...........................C=4.728489228761075; total time=   0.0s
    [CV 10/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 11/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 12/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 13/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 14/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 15/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 16/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 17/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 18/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 19/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 20/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 21/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 22/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 23/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 24/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 25/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 26/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 27/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 28/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 29/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 30/30] END ..........................C=4.728489228761075; total time=   0.0s
    [CV 1/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 2/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 3/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 4/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 5/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 6/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 7/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 8/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 9/30] END ...........................C=6.005177529385341; total time=   0.0s
    [CV 10/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 11/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 12/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 13/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 14/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 15/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 16/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 17/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 18/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 19/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 20/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 21/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 22/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 23/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 24/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 25/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 26/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 27/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 28/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 29/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 30/30] END ..........................C=6.005177529385341; total time=   0.0s
    [CV 1/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 2/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 3/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 4/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 5/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 6/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 7/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 8/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 9/30] END ..........................C=7.6265706475730735; total time=   0.0s
    [CV 10/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 11/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 12/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 13/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 14/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 15/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 16/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 17/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 18/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 19/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 20/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 21/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 22/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 23/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 24/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 25/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 26/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 27/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 28/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 29/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 30/30] END .........................C=7.6265706475730735; total time=   0.0s
    [CV 1/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 2/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 3/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 4/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 5/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 6/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 7/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 8/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 9/30] END ............................C=9.68573860769385; total time=   0.0s
    [CV 10/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 11/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 12/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 13/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 14/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 15/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 16/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 17/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 18/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 19/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 20/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 21/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 22/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 23/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 24/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 25/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 26/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 27/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 28/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 29/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 30/30] END ...........................C=9.68573860769385; total time=   0.0s
    [CV 1/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 2/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 3/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 4/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 5/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 6/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 7/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 8/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 9/30] END ..........................C=12.300880266076671; total time=   0.0s
    [CV 10/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 11/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 12/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 13/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 14/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 15/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 16/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 17/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 18/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 19/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 20/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 21/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 22/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 23/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 24/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 25/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 26/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 27/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 28/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 29/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 30/30] END .........................C=12.300880266076671; total time=   0.0s
    [CV 1/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 2/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 3/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 4/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 5/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 6/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 7/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 8/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 9/30] END ...........................C=15.62210807549156; total time=   0.0s
    [CV 10/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 11/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 12/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 13/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 14/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 15/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 16/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 17/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 18/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 19/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 20/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 21/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 22/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 23/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 24/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 25/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 26/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 27/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 28/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 29/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 30/30] END ..........................C=15.62210807549156; total time=   0.0s
    [CV 1/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 2/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 3/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 4/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 5/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 6/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 7/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 8/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 9/30] END ...........................C=19.84006473060141; total time=   0.0s
    [CV 10/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 11/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 12/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 13/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 14/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 15/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 16/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 17/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 18/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 19/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 20/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 21/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 22/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 23/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 24/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 25/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 26/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 27/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 28/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 29/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 30/30] END ..........................C=19.84006473060141; total time=   0.0s
    [CV 1/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 2/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 3/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 4/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 5/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 6/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 7/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 8/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 9/30] END ..........................C=25.196866300777252; total time=   0.0s
    [CV 10/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 11/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 12/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 13/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 14/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 15/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 16/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 17/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 18/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 19/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 20/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 21/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 22/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 23/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 24/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 25/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 26/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 27/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 28/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 29/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 30/30] END .........................C=25.196866300777252; total time=   0.0s
    [CV 1/30] END ........................................C=32.0; total time=   0.0s
    [CV 2/30] END ........................................C=32.0; total time=   0.0s
    [CV 3/30] END ........................................C=32.0; total time=   0.0s
    [CV 4/30] END ........................................C=32.0; total time=   0.0s
    [CV 5/30] END ........................................C=32.0; total time=   0.0s
    [CV 6/30] END ........................................C=32.0; total time=   0.0s
    [CV 7/30] END ........................................C=32.0; total time=   0.0s
    [CV 8/30] END ........................................C=32.0; total time=   0.0s
    [CV 9/30] END ........................................C=32.0; total time=   0.0s
    [CV 10/30] END .......................................C=32.0; total time=   0.0s
    [CV 11/30] END .......................................C=32.0; total time=   0.0s
    [CV 12/30] END .......................................C=32.0; total time=   0.0s
    [CV 13/30] END .......................................C=32.0; total time=   0.0s
    [CV 14/30] END .......................................C=32.0; total time=   0.0s
    [CV 15/30] END .......................................C=32.0; total time=   0.0s
    [CV 16/30] END .......................................C=32.0; total time=   0.0s
    [CV 17/30] END .......................................C=32.0; total time=   0.0s
    [CV 18/30] END .......................................C=32.0; total time=   0.0s
    [CV 19/30] END .......................................C=32.0; total time=   0.0s
    [CV 20/30] END .......................................C=32.0; total time=   0.0s
    [CV 21/30] END .......................................C=32.0; total time=   0.0s
    [CV 22/30] END .......................................C=32.0; total time=   0.0s
    [CV 23/30] END .......................................C=32.0; total time=   0.0s
    [CV 24/30] END .......................................C=32.0; total time=   0.0s
    [CV 25/30] END .......................................C=32.0; total time=   0.0s
    [CV 26/30] END .......................................C=32.0; total time=   0.0s
    [CV 27/30] END .......................................C=32.0; total time=   0.0s
    [CV 28/30] END .......................................C=32.0; total time=   0.0s
    [CV 29/30] END .......................................C=32.0; total time=   0.0s
    [CV 30/30] END .......................................C=32.0; total time=   0.0s





    GridSearchCV(cv=30,
                 estimator=SVC(kernel='linear', probability=True,
                               random_state=61658),
                 param_grid={'C': array([3.12500000e-02, 3.96874749e-02, 5.04030614e-02, 6.40118475e-02,
           8.12949950e-02, 1.03244578e-01, 1.31120532e-01, 1.66522970e-01,
           2.11484039e-01, 2.68584560e-01, 3.41102176e-01, 4.33199490e-01,
           5.50163004e-01, 6.98706575e-01, 8.87356790e-01, 1.12694241e+00,
           1.43121596e+00, 1.81764312e+00, 2.30840531e+00, 2.93167289e+00,
           3.72322222e+00, 4.72848923e+00, 6.00517753e+00, 7.62657065e+00,
           9.68573861e+00, 1.23008803e+01, 1.56221081e+01, 1.98400647e+01,
           2.51968663e+01, 3.20000000e+01])},
                 scoring='roc_auc', verbose=5)




```python
from sklearn.metrics import roc_auc_score
print(grid1.best_params_,grid1.best_score_,roc_auc_score(y_ts, grid1.predict_proba(X_ts)[:,1]))
print(grid2.best_params_,grid2.best_score_,roc_auc_score(y_ts, grid2.predict_proba(X_ts)[:,1]))
```

    {'class_weight': None, 'criterion': 'entropy', 'max_leaf_nodes': 10} 0.8875 0.788
    {'C': 0.10324457849870658} 1.0 0.916



```python
plt.figure(figsize=(15,7))

plt.subplot(1,2,1)
plt.plot(X[y==0,0],X[y==0,1],'C0o')
plt.plot(X[y==1,0],X[y==1,1],'C3o')

```




    [<matplotlib.lines.Line2D at 0x7f1b0d95b6a0>]




    
![png](DT%20vs%20SVM%20Linear_files/DT%20vs%20SVM%20Linear_8_1.png)
    



```python
plt.figure(figsize=(15,7))

plt.subplot(1,2,1)
plt.plot(X[y==0,0],X[y==0,1],'C0o')
plt.plot(X[y==1,0],X[y==1,1],'C3o')

cm = plt.cm.RdBu_r

h = .02  # step size in the mesh
x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                     np.arange(y_min, y_max, h))

Z = grid1.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]
Z = Z.reshape(xx.shape)
plt.contourf(xx, yy, Z, cmap=cm, alpha=.3)

plt.title(f'Árvore de decisão\nAUC: {roc_auc_score(y_ts,grid1.predict_proba(X_ts)[:,1]):.2f}',fontsize=16)

plt.subplot(1,2,2)
plt.plot(X[y==0,0],X[y==0,1],'C0o')
plt.plot(X[y==1,0],X[y==1,1],'C3o')

cm = plt.cm.RdBu_r

h = .02  # step size in the mesh
x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                     np.arange(y_min, y_max, h))

Z = grid2.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]
Z = Z.reshape(xx.shape)
plt.contourf(xx, yy, Z, cmap=cm, alpha=.3)

plt.title(f'SVM Linear\nAUC: {roc_auc_score(y_ts,grid2.predict_proba(X_ts)[:,1]):.2f}',fontsize=16)
```




    Text(0.5, 1.0, 'SVM Linear\nAUC: 0.92')




    
![png](DT%20vs%20SVM%20Linear_files/DT%20vs%20SVM%20Linear_9_1.png)
    



```python

```
